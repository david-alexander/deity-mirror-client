import React from 'react';
import withRedux from '../utils/withRedux';
import { initStore } from '../redux/store';
import Search from '../containers/Search';
import Entity from '../containers/Entity';
import Header from '../components/Header';
import About from './../containers/About';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import createHistory from 'history/createBrowserHistory'
import styles from './index.scss';
import getConfig from 'next/config';
const {publicRuntimeConfig} = getConfig();

const Index = () => {
  if (typeof window === 'undefined') {
    return <div/>;
  }
  const history = createHistory();
  let menu = publicRuntimeConfig.menu;

  return (
    <Router history={history}>
      <div>
        <Header menu={menu}/>
        <main className={styles.wrapper}>
          <div className="container-fluid">
            <Switch>
              <Route path="/" exact component={Search} />
              <Route path="/about" component={About} />
              <Route component={Entity} />
            </Switch>
          </div>
        </main>
      </div>
    </Router>
  );
};

export default withRedux(initStore, null, null)(Index)
