# Deity Mirror Client

This project shows how it feels to work with real Deity software before it is publicly available.

Built using [Next.js](https://learnnextjs.com/) with its great documentation and a wrapper for all webpack/babel complexity.

It uses external node api gateway and port some of the components of the original client.

## Requirements

[Node v8 LTS](https://nodejs.org/en/download/)

## How to start

```
git clone git@bitbucket.org:deity-core/deity-mirror-client.git
cd deity-mirror-client
npm install
npm run dev
```

Go to ``localhost:3000`` in your browser.

## API gateway

Mirror client uses [demo.deity.io](https://demo.deity.io) as default API gateway.

To change it create ``config/debug.json`` with following content:

```
    {
      "apiGatewayUrl": "http://example.com/"
    }
```

## Contributing

Similar to github flow:

For the repository (``+`` sign on the left side panel in bitbucket).

Commit changes to your forked repository.

Make a pull request changing the target to deity-core/deity-mirror-client.

Thanks !

## Easy picks
* Update styles for component / containers
* Add basic SEO setup with [react-helmet](https://github.com/nfl/react-helmet)
* Add another route to static page like team
* ~~Add breadcrumbs on category page~~
* Display sku/description/breadcrumbs on product page
* ~~Generate menu based on config setup~~
* Add another Redux reducer and use it with connect
* Add pagination / infinite loader for product list

## Limitations

There's no server side rendering to keep things simple, refreshing the page is not a good idea.

Always start with homepage for full page load.
