const withSass = require('@zeit/next-sass');
const config = require('config');

const {menu} = config

module.exports = withSass({
  cssModules: true,
  publicRuntimeConfig: {menu}
});
