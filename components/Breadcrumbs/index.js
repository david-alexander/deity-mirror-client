import React, { Component } from 'react';
import { Link } from "react-router-dom";
import styles from './styles.scss';

export default class Breadcrumbs extends Component {
    render () {
        const { data } = this.props;

        return (
            <div className={styles.breadcrumbs}>
                <div className="row">
                    <div className="col-sm-12">
                        <ul>
                            <li>
                                <a href="/">Home</a>
                            </li>
                            {data.map((breadcrumb, i) => (
                                <li className="breadcrumb" key={i}>
                                    {breadcrumb.urlKey ? (
                                        <Link to={`/${breadcrumb.urlPath}`}><span className="breadcrumb-link">{breadcrumb.name}</span></Link>
                                    ) : (
                                        <span className="breadcrumb-label">{breadcrumb.name}</span>
                                    )}
                                </li>)
                            )}
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}
