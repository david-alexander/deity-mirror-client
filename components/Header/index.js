import React from 'react';
import styles from './styles.scss';
import { Link } from "react-router-dom";

/**
 * @todo: render menu based on config setup
 */
 export default ({ menu }) => (
  <header className={styles.header}>
    <div className="container-fluid">
      <div className="navbar-header">
        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span className="sr-only">Toggle navigation</span>
          <span className="icon-bar" />
          <span className="icon-bar" />
          <span className="icon-bar" />
        </button>
        <Link to="/">
            <img src="/static/logo.png" alt="deity" />
        </Link>
      </div>
      <div id="navbar" className="navbar-collapse collapse">
        <ul className="nav navbar-nav navbar-right">
          {menu.map(item => (
            <li key={item.index}>
              <Link to={item.link}>
                {item.label}
              </Link>
            </li>
          ))}
        </ul>
      </div>
    </div>
  </header>
);
