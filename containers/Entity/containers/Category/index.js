import React, { Component } from 'react';
import styles from './styles.scss';
import Breadcrumbs from "../../../../components/Breadcrumbs";

class Category extends Component {

  getDescription() {
    return this.props.custom_attributes && this.props.custom_attributes.description;
  }
  getBreadcrumbs() {
      return this.props.extension_attributes && this.props.extension_attributes.breadcrumbs;
  }

  render () {
    const { name } = this.props;
    const description = this.getDescription();
    const breadcrumbsData = this.getBreadcrumbs();

    return (
      <div className={styles.category}>
        <Breadcrumbs data={breadcrumbsData} />
        <h1>{name}</h1>
        {description && <p dangerouslySetInnerHTML={{ __html: description }} />}
      </div>
    )
  }
}

export default Category;
