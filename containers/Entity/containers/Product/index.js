import React, { Component } from 'react';
import styles from './styles.scss';
import Breadcrumbs from "../../../../components/Breadcrumbs";

class Product extends Component {

    getBreadcrumbs() {
        return this.props.extension_attributes && this.props.extension_attributes.breadcrumbs;
    }

    render () {
    const { name, price, extension_attributes: { thumbnail_resized_url } } = this.props;
    const breadcrumbsData = this.getBreadcrumbs();

    return (
      <div className={styles.product}>
        <Breadcrumbs data={breadcrumbsData} />
        <div className="row">
          <div className="col-sm-6">
            <img src={thumbnail_resized_url} alt={name} />
          </div>
          <div className="col-sm-6">
            <h1>{name}</h1>
            <strong>{price}</strong>
          </div>
        </div>
      </div>
    )
  }
}

export default Product;
